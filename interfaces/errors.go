package interfaces

import (
	"errors"
	"fmt"
	"strings"
)

var (
	ErrNoSuchVersion    = errors.New("requested version is not available")
	ErrVersionCollision = errors.New("specified version already exists")
	ErrNoVersion        = errors.New("no version has been selected as active")
	ErrBadImpl          = errors.New("bad implementation of IArchiver passed to factory")
	ErrBadHash          = errors.New("hashes do not match")
	ErrFileNotFound     = errors.New("file does not exist in this version")
)

type MultiError []error

func (me MultiError) Error() string {
	var b strings.Builder

	b.WriteString("Multiple errors:\n")

	for _, e := range me {
		fmt.Fprintf(&b, "\t- %s\n", e.Error())
	}

	return b.String()
}
