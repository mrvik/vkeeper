package interfaces

import (
	"archive/tar"
	"archive/zip"
	"crypto/sha256"
	"io"
	"io/fs"
	"time"
)

type Checksum [sha256.Size]byte

type IVersioner interface {
	// GetVersions returns an slice of versions present in the archive
	GetVersions() []IVersion
	// SetVersion establishes version as the current version.
	// If the current version is not present on the list returned by GetVersions, ErrNoSuchVersion will be returned.
	SetVersion(version string) error
	// NewVersion creates a new empty version in the archive. If the name specified is already present, ErrVersionCollision will be returned.
	NewVersion(name string) error
	// DeleteVersion deletes the specified version from the archive. It returns ErrNoSuchVersion if the version cannot be found.
	// It implicitly calls SetVersion over the newly created version.
	DeleteVersion(version string) error
}

// IArchiveFS is a mandatory interface that allows modifying the archive files.
// A version must be already selected to run any of these commands.
type IArchiveFS interface {
	// Open opens the specified file on the current version.
	// It will always return a fs.PathError as specified by fs.FS.
	// Open must be safe to call concurrently.
	Open(filename string) (fs.File, error)
	// Store a new file in the archive. Filename will be added to the index replacing any file with the specified filename.
	// Must be safe for concurrent use.
	Store(filename string, hash Checksum, file fs.File) error
	// Delete removes a file from index. Blob is left on the tree unless GC is called.
	// Must be safe for concurrent use.
	Delete(filename string) error
	// GC runs a garbage collection by looking up orphan hashes.
	// It won't do the heavy lifting to remove files. Close **must** be called in order to actually do changes.
	GC() error
	// List returns a map of files and their checksum.
	List() (map[string]Checksum, error)
	// Check for internal consistency inside the archive.
	// Returns error if any error is encontered on the internal packaging.
	Check() error
}

// IPackager describes an interface allowing an archiver to package an user-readable archive.
// Package files will be the ones present on a determinate version.
type IPackager interface {
	// PackageZIP generates a new zip file from the files present on the active version.
	PackageZIP(to *zip.Writer) error
	// PackageTAR writes all files present on the active version to the specified tar writer.
	PackageTAR(to *tar.Writer) error
}

type IArchiveCloser interface {
	// Implementation of io.WriterTo flushes current archiver state to the specified writer.
	io.WriterTo
	// Close closes the file w/o writing any pending changes to archive.
	Close() error
}

// IArchiver describes the interface expected from an archiver.
// It comprehends also IVersioner, so SetVersion must be called either implicit or explicitly.
// If no version was selected, ErrNoVersion will be returned wrapped in a fs.PathError if required to by fs.FS.
type IArchiver interface {
	IVersioner
	IArchiveFS
	IPackager
	IArchiveCloser
}

// IArchiverFactory is a factory that controls the livecycle of a concrete IArchiver implementation.
// Some factories may enforce using the type they're actually implementing. In this case, ErrBadImpl should be returned.
type IArchiverFactory interface {
	// OpenArchiver opens a new IArchiver for the selected file.
	// If the file doesn't exist, createMissing defines if a new empty archiver should be created.
	OpenArchiver(filepath string, createMissing bool) (IArchiver, error)
	// Close archiver closes the passed archiver optionally flushing changes to the selected filepath.
	// Implementations that use a single file **must** take care of collisions or file locking on some operating systems.
	// Caller must **not** use the passed IArchiver after calling this method.
	CloseArchiver(filepath string, flush bool, archiver IArchiver) error
}

type IVersion interface {
	Name() string
	Owner() string
	Created() time.Time
	NumFiles() int64
}
