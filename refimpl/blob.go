package refimpl

import (
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path"
	"runtime"
	"sync"
	"time"
	"vkeeper/interfaces"

	"github.com/schollz/progressbar/v3"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
)

type checkOrder struct {
	file   fs.File
	target interfaces.Checksum
}

func CheckBlobs(logEntry *logrus.Entry, blobDir fs.FS) error {
	var (
		workers     = runtime.NumCPU()
		blobs, err  = fs.ReadDir(blobDir, ".")
		ctx, cancel = context.WithCancel(context.Background())
		eg, egctx   = errgroup.WithContext(ctx)
	)

	defer cancel()

	if workers == 0 {
		workers = 1 // At least 1 worker.
	}

	if err != nil {
		return fmt.Errorf("read blob names: %w", err)
	}

	log := logEntry.WithFields(logrus.Fields{
		"blobCount": len(blobs),
		"workers":   workers,
	})

	progress := progressbar.NewOptions64(int64(len(blobs)),
		progressbar.OptionSetWriter(os.Stderr),
		progressbar.OptionSetWidth(10),
		progressbar.OptionThrottle(65*time.Millisecond),
		progressbar.OptionShowCount(),
		progressbar.OptionShowIts(),
		progressbar.OptionSetItsString("blobs"),
		progressbar.OptionOnCompletion(func() {
			os.Stderr.Write([]byte{'\n'})
		}),
		progressbar.OptionSpinnerType(14),
		progressbar.OptionFullWidth(),
		progressbar.OptionSetRenderBlankState(true),
	)
	blobChannel := make(chan checkOrder, workers*2)
	errorChannel := make(chan error, workers*2)

	for i := 0; i < workers; i++ {
		worker := i

		eg.Go(func() error {
			hasher := sha256.New()
			log := logrus.WithField("worker", worker)

			for {
				var (
					current checkOrder
					ok      bool
				)

				select {
				case <-egctx.Done():
					return egctx.Err()
				case current, ok = <-blobChannel:
					if !ok {
						return nil
					}

					hasher.Reset()
				}

				if err := func(order checkOrder) error {
					defer order.file.Close()

					log.Debugf("Hashing blob %x", order.target)

					if _, err := io.Copy(hasher, order.file); err != nil {
						return fmt.Errorf("hash blob %x again: %w", order.target, err)
					}

					nh := hasher.Sum(nil)

					if !bytes.Equal(nh, order.target[:]) {
						return fmt.Errorf("%w: expected=%x, got=%x", interfaces.ErrBadHash, order.target, nh)
					}

					return nil
				}(current); err != nil {
					if !errors.Is(err, interfaces.ErrBadHash) {
						return err
					}

					errorChannel <- err
				}

				progress.Add(1)
			}
		})
	}

	for _, blob := range blobs {
		var (
			target interfaces.Checksum
			name   = blob.Name()
		)

		sha, err := hex.DecodeString(name)
		if err != nil {
			return fmt.Errorf("decode blob name %q: %w", name, err)
		}

		copy(target[:], sha)

		blobFile, err := blobDir.Open(name)
		if err != nil {
			return fmt.Errorf("open blob %q: %w", name, err)
		}

		blobChannel <- checkOrder{
			file:   blobFile,
			target: target,
		}
	}

	close(blobChannel)

	log.Debug("Waiting for workers")

	var finalError error

	go func() {
		if err := eg.Wait(); err != nil {
			finalError = fmt.Errorf("wait for checker workers: %w", err)
		}

		progress.Finish()
		close(errorChannel)
	}()

	errors := make([]error, 0, 1)

	for e := range errorChannel {
		errors = append(errors, e)
	}

	switch {
	case finalError != nil:
		return finalError
	case len(errors) == 0:
		return nil
	default:
		return interfaces.MultiError(errors)
	}
}

// BlobManager allows to keep track of blobs in any given structure.
type BlobManager struct {
	mp  map[interfaces.Checksum]struct{}
	mtx sync.RWMutex
}

func (bm *BlobManager) Populate(entries []fs.DirEntry) {
	blobMap := make(map[interfaces.Checksum]struct{}, len(entries))

	for _, ent := range entries {
		hash, err := hex.DecodeString(path.Base(ent.Name()))
		if err != nil {
			continue
		}

		var i interfaces.Checksum

		copy(i[:], hash)

		blobMap[i] = struct{}{}
	}

	bm.mtx.Lock()
	defer bm.mtx.Unlock()

	bm.mp = blobMap
}

// HasOrRegister returns true if the blob was registered and false if it wan't
// because the blob is already registered.
func (bm *BlobManager) HasOrRegister(ch interfaces.Checksum) bool {
	if bm.Check(ch) {
		return false
	}

	bm.mtx.Lock()
	defer bm.mtx.Unlock()

	if bm.checkNoLock(ch) {
		return false
	}

	bm.mp[ch] = struct{}{}

	return true
}

func (bm *BlobManager) Check(ch interfaces.Checksum) bool {
	bm.mtx.RLock()
	defer bm.mtx.RUnlock()

	return bm.checkNoLock(ch)
}

func (bm *BlobManager) checkNoLock(ch interfaces.Checksum) bool {
	_, ok := bm.mp[ch]
	return ok
}
