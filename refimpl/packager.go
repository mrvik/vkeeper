package refimpl

import (
	"archive/tar"
	"archive/zip"
	"fmt"
	"io"
	"io/fs"
	"vkeeper/interfaces"
)

func PackageTAR(from interfaces.IArchiveFS, to *tar.Writer) error {
	list, err := from.List()
	if err != nil {
		return fmt.Errorf("get file list: %w", err)
	}

	for file := range list {
		if err := writeFileTAR(from, file, to); err != nil {
			return err
		}
	}

	return nil
}

func writeFileTAR(from fs.FS, name string, to *tar.Writer) error {
	file, err := from.Open(name)
	if err != nil {
		return fmt.Errorf("open file %q: %w", name, err)
	}

	stat, err := file.Stat()
	if err != nil {
		return fmt.Errorf("stat file %q: %w", name, err)
	}

	info, err := tar.FileInfoHeader(stat, "")
	if err != nil {
		return fmt.Errorf("generate tar header for %q: %w", name, err)
	}

	info.Name = name

	if err := to.WriteHeader(info); err != nil {
		return fmt.Errorf("write header for %q into tar file: %w", name, err)
	}

	var buf [4096]byte

	if _, err := io.CopyBuffer(to, file, buf[:]); err != nil {
		return fmt.Errorf("copy file contents (%q) to archive: %w", name, err)
	}

	return nil
}

func PackageZIP(from interfaces.IArchiveFS, to *zip.Writer) error {
	list, err := from.List()
	if err != nil {
		return fmt.Errorf("get file list: %w", err)
	}

	for file := range list {
		if err := writeFileZIP(from, file, to); err != nil {
			return err
		}
	}

	return nil
}

func writeFileZIP(from fs.FS, name string, to *zip.Writer) error {
	file, err := from.Open(name)
	if err != nil {
		return fmt.Errorf("open file %q: %w", name, err)
	}

	stat, err := file.Stat()
	if err != nil {
		return fmt.Errorf("stat file %q: %w", name, err)
	}

	info, err := zip.FileInfoHeader(stat)
	if err != nil {
		return fmt.Errorf("generate zip header for %q: %w", name, err)
	}

	info.Name = name

	writer, err := to.CreateHeader(info)
	if err != nil {
		return fmt.Errorf("write header for %q into zip file: %w", name, err)
	}

	var buf [4096]byte

	if _, err := io.CopyBuffer(writer, file, buf[:]); err != nil {
		return fmt.Errorf("copy file contents (%q) to archive: %w", name, err)
	}

	return nil
}
