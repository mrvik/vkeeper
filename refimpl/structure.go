package refimpl

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"sync"
)

const (
	ObjectStore    = "objects"
	DirectoryStore = "directory"
)

func ReadVersions(dir fs.FS) ([]Version, error) {
	versionDirs, err := fs.ReadDir(dir, ".")
	if err != nil {
		return nil, fmt.Errorf("read versions from dir: %w", err)
	}

	versions := make([]Version, len(versionDirs))

	for v, file := range versionDirs {
		version, err := readVersion(dir, file.Name())
		if err != nil {
			return nil, fmt.Errorf("read version file %q: %w", file.Name(), err)
		}

		versions[v] = version
	}

	return versions, nil
}

func readVersion(from fs.FS, name string) (Version, error) {
	var version Version

	file, err := from.Open(name)
	if err != nil {
		return version, fmt.Errorf("open version file %q: %w", name, err)
	}

	defer file.Close()

	if err := json.NewDecoder(file).Decode(&version); err != nil {
		return version, fmt.Errorf("decode json for version %q: %w", name, err)
	}

	version.mtx = new(sync.RWMutex)

	return version, nil
}
