package refimpl

import (
	"fmt"
	"io/fs"
	"sync"
	"time"
	"vkeeper/interfaces"
)

// Version holds data for an specific version. This is a reference implementation
// of what a "Version" should provide and it's public methods are safe to be concurrently called.
type Version struct {
	VersionName string `json:"name"`

	Ctime     time.Time `json:"ctime"`
	CreatedBy string    `json:"owner"`

	// File index for this version.
	Files map[string]*File
	// Version mutex that **must** be hold before using the files map.
	mtx *sync.RWMutex
}

func NewEmptyVersion(name, owner string) Version {
	return Version{
		VersionName: name,
		Ctime:       time.Now(),
		CreatedBy:   owner,

		Files: make(map[string]*File),
		mtx:   new(sync.RWMutex),
	}
}

func (v Version) Name() string {
	return v.VersionName
}

func (v Version) Owner() string {
	return v.CreatedBy
}

func (v Version) Created() time.Time {
	return v.Ctime
}

func (v Version) NumFiles() int64 {
	v.mtx.RLock()
	defer v.mtx.RUnlock()

	return int64(len(v.Files))
}

func (v *Version) AddFile(filename string, hash interfaces.Checksum, stat fs.FileInfo) {
	v.mtx.Lock()
	defer v.mtx.Unlock()

	v.Files[filename] = &File{
		SHA256: hash,
		FileInfo: FileInfo{
			Mode:     stat.Mode(),
			Mtime:    stat.ModTime(),
			FileSize: stat.Size(),
		},
	}
}

func (v *Version) RemoveFile(filename string) {
	v.mtx.Lock()
	defer v.mtx.Unlock()

	delete(v.Files, filename)
}

func (v *Version) List() map[string]interfaces.Checksum {
	v.mtx.RLock()
	defer v.mtx.RUnlock()

	fileList := make(map[string]interfaces.Checksum, len(v.Files))

	for f, fc := range v.Files {
		fileList[f] = fc.SHA256
	}

	return fileList
}

func (v *Version) FindFile(filename string) (File, error) {
	v.mtx.RLock()
	defer v.mtx.RUnlock()

	file, ok := v.Files[filename]
	if !ok {
		return File{}, fmt.Errorf("lookup file %q in version %s: %w", filename, v.VersionName, interfaces.ErrFileNotFound)
	}

	return *file, nil
}

type File struct {
	SHA256   interfaces.Checksum `json:"sha256"`
	FileInfo FileInfo            `json:"info"`
}

func (f *File) UpdateInfo(ni FileInfo) {
	f.FileInfo = ni
}

type FileInfo struct {
	Mode     fs.FileMode
	Mtime    time.Time
	FileSize int64
}

func (f File) Name() string {
	return "" // File doesn't hold it's own name
}

func (f File) Size() int64 {
	return f.FileInfo.FileSize
}

func (f File) Mode() fs.FileMode {
	return f.FileInfo.Mode
}

func (f File) ModTime() time.Time {
	return f.FileInfo.Mtime
}

func (f File) IsDir() bool {
	return false // We don't do that here
}

func (f File) Sys() interface{} {
	return nil // I'm the sys
}
