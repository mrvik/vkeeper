package refimpl

import "io/fs"

type FileInfoWrapper struct {
	fs.File
	statSource fs.FileInfo
}

func NewFileInfoWrapper(blobFile fs.File, statSource fs.FileInfo) fs.File {
	return FileInfoWrapper{
		blobFile,
		statSource,
	}
}

func (b FileInfoWrapper) Stat() (fs.FileInfo, error) {
	return b.statSource, nil
}
