package refimpl

import (
	"fmt"
	"os"
	"os/user"
	"vkeeper/interfaces"
)

func NewVersion(name string, versioner interfaces.IVersioner, newVersionFn func(name, owner string) interfaces.IVersion) (interfaces.IVersion, error) {
	versions := versioner.GetVersions()

	for _, v := range versions {
		if v.Name() == name {
			return nil, fmt.Errorf("create version %s: %w", name, interfaces.ErrVersionCollision)
		}
	}

	username := os.Getenv("USER")
	if u, _ := user.Current(); u != nil {
		username = u.Name
	}

	v := newVersionFn(name, username)

	return v, nil
}
