package jsondto

import (
	"time"
	"vkeeper/interfaces"
)

type Version struct {
	VersionName string    `json:"versionName"`
	FileNumber  int64     `json:"fileNumber"`
	Owner       string    `json:"owner"`
	CreatedAt   time.Time `json:"ctime"`
}

func NewVersion(v interfaces.IVersion) Version {
	return Version{
		VersionName: v.Name(),
		FileNumber:  v.NumFiles(),
		Owner:       v.Owner(),
		CreatedAt:   v.Created(),
	}
}

func VersionsSlice(versions []interfaces.IVersion) []Version {
	nver := make([]Version, len(versions))

	for i, v := range versions {
		nver[i] = NewVersion(v)
	}

	return nver
}
