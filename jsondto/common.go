package jsondto

type CommonResponse struct {
	HasError     bool        `json:"hasError"`
	ErrorMessage string      `json:"errorMessage,omitempty"`
	Versions     []Version   `json:"versionList,omitempty"`
	Changes      DiffSummary `json:"changes"`
}

type DiffSummary struct {
	Remove  []string `json:"remove,omitempty"`
	Extract []string `json:"extract,omitempty"`
}

func Error(err error) CommonResponse {
	return CommonResponse{
		HasError:     true,
		ErrorMessage: err.Error(),
	}
}

func WithChanges(ds DiffSummary) CommonResponse {
	return CommonResponse{
		Changes: ds,
	}
}
