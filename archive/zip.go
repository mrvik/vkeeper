package archive

import (
	"fmt"
	"io"
	"os"
	"path"
	"vkeeper/interfaces"
	"vkeeper/ziparchive"

	"github.com/sirupsen/logrus"
)

type zipFactory struct {
	openFiles map[string]io.Closer
	log       *logrus.Entry
}

func newZIPFactory() interfaces.IArchiverFactory {
	return zipFactory{
		openFiles: make(map[string]io.Closer),
		log: logrus.WithFields(logrus.Fields{
			"factory": "zipFactory",
		}),
	}
}

// OpenArchiver opens a new IArchiver for the selected file.
// If the file doesn't exist, createMissing defines if a new empty archiver should be created.
func (z zipFactory) OpenArchiver(filepath string, createMissing bool) (interfaces.IArchiver, error) {
	file, err := os.OpenFile(filepath, os.O_RDONLY, 0o400)
	if os.IsNotExist(err) {
		if !createMissing {
			return nil, fmt.Errorf("open zip archive at %q: %w", filepath, err)
		}

		return ziparchive.NewEmptyReader(filepath), nil
	}

	z.openFiles[filepath] = file

	ar, err := ziparchive.NewReader(filepath, file)
	if err != nil {
		return nil, fmt.Errorf("create zip archive reader for %q: %w", filepath, err)
	}

	return ar, nil
}

// Close archiver closes the passed archiver optionally flushing changes to the selected filepath.
// Implementations that use a single file **must** take care of collisions or file locking on some operating systems.
// Caller must **not** use the passed IArchiver after calling this method.
func (z zipFactory) CloseArchiver(filepath string, flush bool, archiver interfaces.IArchiver) error {
	defer archiver.Close()

	fsFile := z.openFiles[filepath]
	delete(z.openFiles, filepath)

	if fsFile != nil {
		defer fsFile.Close()
	}

	if !flush {
		z.log.WithField("filepath", "filepath").Info("Closed w/o flushing data")

		return nil
	}

	tmpfile, err := os.CreateTemp(path.Dir(filepath), path.Base(filepath))
	if err != nil {
		return fmt.Errorf("create temp file to close: %w", err)
	}

	defer tmpfile.Close()

	log := z.log.WithFields(logrus.Fields{
		"tmpfile":  tmpfile.Name(),
		"destfile": filepath,
	})

	log.Info("Writing archive state to temporary file")

	if _, err := archiver.WriteTo(tmpfile); err != nil {
		return fmt.Errorf("write archiver to tmpfile at %q: %w", tmpfile.Name(), err)
	}

	// Closing it before it's replaced.
	if fsFile != nil {
		fsFile.Close()
	}

	archiver.Close() // From this on, the archiver is closed
	tmpfile.Close()

	if err := os.Rename(tmpfile.Name(), filepath); err != nil {
		log.Warn("Error renaming file. You can do it manually")

		return fmt.Errorf("rename %q to %q: %w", tmpfile.Name(), filepath, err)
	}

	os.Remove(tmpfile.Name())
	log.Info("Archive updated")

	return nil
}
