package archive

import (
	"os"
	"path/filepath"
	"vkeeper/interfaces"
)

type Selector struct {
	factories map[string]interfaces.IArchiverFactory
}

func NewSelector() Selector {
	return Selector{
		factories: map[string]interfaces.IArchiverFactory{
			"zip": newZIPFactory(),
			"dir": newDirArchiveFactory(),
		},
	}
}

func SelectImplementation(filename, defaultImpl string) string {
	if file, _ := os.Lstat(filename); file != nil && file.IsDir() {
		return "dir"
	}

	if filepath.Ext(filename) == ".zip" {
		return "zip"
	}

	return defaultImpl
}

func (s Selector) GetNames() []string {
	names := make([]string, 0, len(s.factories))

	for name := range s.factories {
		names = append(names, name)
	}

	return names
}

func (s Selector) GetFactory(name string) interfaces.IArchiverFactory {
	return s.factories[name]
}
