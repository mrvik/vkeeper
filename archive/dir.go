package archive

import (
	"path/filepath"
	"vkeeper/dirarchive"
	"vkeeper/interfaces"
)

type DirArchiveFactory map[string]interfaces.IArchiver

// OpenArchiver opens a new IArchiver for the selected file.
// If the file doesn't exist, createMissing defines if a new empty archiver should be created.
func (df DirArchiveFactory) OpenArchiver(path string, createMissing bool) (interfaces.IArchiver, error) {
	fp := filepath.Clean(path)

	if existing, ok := df[fp]; ok {
		return existing, nil
	}

	archiver, err := dirarchive.NewDirArchive(fp, createMissing)
	if err != nil {
		return nil, err
	}

	df[fp] = archiver

	return archiver, nil
}

// Close archiver closes the passed archiver optionally flushing changes to the selected filepath.
// Implementations that use a single file **must** take care of collisions or file locking on some operating systems.
// Caller must **not** use the passed IArchiver after calling this method.
func (a DirArchiveFactory) CloseArchiver(filepath string, flush bool, archiver interfaces.IArchiver) error {
	delete(a, filepath)

	defer archiver.Close()

	if !flush {
		return nil
	}

	_, err := archiver.WriteTo(nil)
	return err
}

func newDirArchiveFactory() DirArchiveFactory {
	return make(DirArchiveFactory)
}
