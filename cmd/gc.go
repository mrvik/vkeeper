package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"vkeeper/archive"
	"vkeeper/jsondto"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// gcCmd represents the gc command
var gcCmd = &cobra.Command{
	Use:   "gc",
	Short: "Do garbage collection on archive",
	Long:  `Perform a garbage collection on the specified archive`,
	Run: func(cmd *cobra.Command, args []string) {
		archiveName, _ := cmd.Flags().GetString(ParamArchiveFile)
		implName, _ := cmd.Flags().GetString(ParamImpl)
		log := logrus.WithFields(logrus.Fields{
			"archive":        archiveName,
			"implementation": implName,
		})

		if err := doGC(implName, archiveName); err != nil {
			if jsonOutput {
				json.NewEncoder(os.Stdout).Encode(jsondto.Error(err))
			}

			log.Fatal(err)
		}

		if jsonOutput {
			commandOK()
		}
	},
}

func doGC(implName, archiveName string) error {
	impl := archive.NewSelector().GetFactory(implName)
	if impl == nil {
		return fmt.Errorf("%w: %s", ErrNoFactory, implName)
	}

	archive, err := impl.OpenArchiver(archiveName, false)
	if err != nil {
		return fmt.Errorf("open archiver: %w", err)
	}

	if err := archive.GC(); err != nil {
		return fmt.Errorf("do garbage collection: %w", err)
	}

	if err := impl.CloseArchiver(archiveName, true, archive); err != nil {
		return fmt.Errorf("close modified archiver: %w", err)
	}

	return nil
}

func init() {
	rootCmd.AddCommand(gcCmd)
}
