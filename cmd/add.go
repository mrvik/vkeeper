package cmd

import (
	"context"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"runtime"
	"vkeeper/archive"
	"vkeeper/interfaces"
	"vkeeper/jsondto"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"golang.org/x/sync/errgroup"
)

// addCmd represents the add command
var addCmd = &cobra.Command{
	Use:   "add directories...",
	Short: "Add specified files/directories to the specified archive",
	Long:  `Add files/directories to the specified archive generating a new version`,
	Run: func(cmd *cobra.Command, args []string) {
		versionName, _ := cmd.Flags().GetString(ParamVersion)
		archiveFile, _ := cmd.Flags().GetString(ParamArchiveFile)
		implName, _ := cmd.Flags().GetString(ParamImpl)
		noCreate, _ := cmd.Flags().GetBool(ParamNoCreate)
		chrootDir, _ := cmd.Flags().GetString("chroot")
		ignoreDF, _ := cmd.Flags().GetBool("ignore-dotfiles")
		noError, _ := cmd.Flags().GetBool("ignore-errors")

		log := logrus.WithFields(logrus.Fields{
			"version":        versionName,
			"archive":        archiveFile,
			"implementation": implName,
		})

		if err := doAdd(log, implName, archiveFile, chrootDir, versionName, noCreate, ignoreDF, noError, args); err != nil {
			if jsonOutput {
				json.NewEncoder(os.Stdout).Encode(jsondto.Error(err))
			}

			log.Fatal(err)

			return
		}

		if jsonOutput {
			commandOK()
		}
	},
}

func init() {
	rootCmd.AddCommand(addCmd)

	cwd, _ := os.Getwd()

	addCmd.Flags().Bool(ParamNoCreate, false, "Exit with error if the specified archive file doesn't exist")
	addCmd.Flags().StringP(ParamVersion, "v", "", "Set version name for operation")
	addCmd.Flags().StringP("chroot", "C", cwd, "Root directory for archive")
	addCmd.Flags().Bool("ignore-dotfiles", false, "Ignore dotfiles when storing into archive")
	addCmd.Flags().Bool("ignore-errors", false, "Ignore errors while storing files in the archive")
}

func doAdd(log *logrus.Entry, implName, archiveFile, chrootDir, versionName string, noCreate, ignoreDF, ignoreErrors bool, args []string) error {
	if versionName == "" {
		return fmt.Errorf("%w: version-name is required for add", ErrMissingRequiredFlag)
	}

	if len(args) == 0 {
		return fmt.Errorf("%w: refusing to create an empty version", ErrMissingRequiredFlag)
	}

	impl := archive.NewSelector().GetFactory(implName)
	if impl == nil {
		return fmt.Errorf("%w: %s", ErrNoFactory, implName)
	}

	archive, err := impl.OpenArchiver(filepath.ToSlash(filepath.Clean(archiveFile)), !noCreate)
	if err != nil {
		return fmt.Errorf("open archiver: %w", err)
	}

	if err := archive.NewVersion(versionName); err != nil {
		return fmt.Errorf("create new version: %w", err)
	}

	log.Infof("Filling version with files from %+v", args)

	if err := fillVersion(log, archive, ignoreDF, ignoreErrors, os.DirFS(filepath.ToSlash(filepath.Clean(chrootDir))), args); err != nil {
		return fmt.Errorf("fill version with files: %w", err)
	}

	if err := impl.CloseArchiver(archiveFile, true, archive); err != nil {
		return fmt.Errorf("close archiver: %w", err)
	}

	return nil
}

func fillVersion(log *logrus.Entry, archive interfaces.IArchiveFS, ignoreDF, ignoreErrors bool, root fs.FS, rootElements []string) error {
	var (
		cpuCount    = runtime.NumCPU()
		dirs        []string
		ctx, cancel = context.WithCancel(context.Background())
		eg, egctx   = errgroup.WithContext(ctx)
		files       = make(chan string)
	)

	defer cancel()

	eg.Go(func() error {
		defer close(files)

		for _, el := range rootElements {
			element := filepath.ToSlash(filepath.Clean(el))

			switch stat, err := fs.Stat(root, element); {
			case err != nil:
				return fmt.Errorf("get info for %q: %w", element, err)
			case stat.IsDir():
				dirs = append(dirs, element)
				continue
			}

			select {
			case <-egctx.Done():
				return egctx.Err()
			case files <- element:
			}
		}

		for _, dir := range dirs {
			log.WithField("base", dir).Debug("Walking dir")

			if err := fs.WalkDir(root, dir, func(name string, entry fs.DirEntry, err error) error {
				if err != nil {
					return err
				}

				if !entry.Type().IsRegular() {
					return nil
				}

				select {
				case <-egctx.Done():
					return egctx.Err()
				case files <- name:
				}

				return nil
			}); err != nil {
				return fmt.Errorf("walk dir %q: %w", dir, err)
			}
		}

		return nil
	})

	for i := 0; i < cpuCount; i++ {
		wnum := i
		eg.Go(func() error {
			log := log.WithField("worker", wnum)
			for filename := range files {
				if ignoreDF && (isDotFile(filename)) {
					log.Debugf("Ignore dotfile %q (%t)", filename, isDotFile(filename))

					continue
				}

				log.Debugf("Store %s", filename)

				if err := storeElement(archive, root, filename); !ignoreErrors && err != nil {
					return err
				}
			}

			return nil
		})
	}

	if err := eg.Wait(); err != nil {
		return fmt.Errorf("wait for goroutines: %w", err)
	}

	return nil
}

// storeElement calls store over the selected file on the "into" archiver FS.
func storeElement(into interfaces.IArchiveFS, root fs.FS, filename string) error {
	file, err := root.Open(filename)
	if err != nil {
		return fmt.Errorf("open %q to store: %w", filename, err)
	}

	hasher := sha256.New()

	if _, err := io.Copy(hasher, file); err != nil {
		return fmt.Errorf("hash %q: %w", filename, err)
	}

	var hash interfaces.Checksum

	copy(hash[:], hasher.Sum(nil))

	file.(io.Seeker).Seek(io.SeekStart, 0)

	if err := into.Store(filename, hash, file); err != nil {
		return fmt.Errorf("store %q (hash=%x): %w", filename, hash, err)
	}

	return nil
}
