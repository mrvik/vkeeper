package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"vkeeper/archive"
	"vkeeper/jsondto"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// checkCmd represents the check command
var checkCmd = &cobra.Command{
	Use:   "check",
	Short: "Check for internal archive consistency",
	Long:  `Check for internal archive consistency by recomputing checksums`,
	Run: func(cmd *cobra.Command, args []string) {
		archiveName, _ := cmd.Flags().GetString(ParamArchiveFile)
		implName, _ := cmd.Flags().GetString(ParamImpl)
		log := logrus.WithFields(logrus.Fields{
			"archive":        archiveName,
			"implementation": implName,
		})

		if err := doCheck(implName, archiveName); err != nil {
			if jsonOutput {
				json.NewEncoder(os.Stdout).Encode(jsondto.Error(err))
			}

			log.Fatal(err)
		}

		if jsonOutput {
			commandOK()
		}
	},
}

func init() {
	rootCmd.AddCommand(checkCmd)
}

func doCheck(implName, archiveName string) error {
	impl := archive.NewSelector().GetFactory(implName)
	if impl == nil {
		return fmt.Errorf("%w: %s", ErrNoFactory, implName)
	}

	archive, err := impl.OpenArchiver(archiveName, false)
	if err != nil {
		return fmt.Errorf("open archiver: %w", err)
	}

	defer impl.CloseArchiver(archiveName, false, archive)

	if err := archive.Check(); err != nil {
		return fmt.Errorf("check internal consistency: %w", err)
	}

	return nil
}
