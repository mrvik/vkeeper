package cmd

import (
	"archive/tar"
	"archive/zip"
	"fmt"
	"io"
	"os"
	"vkeeper/archive"
	"vkeeper/interfaces"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// packageCmd represents the package command
var packageCmd = &cobra.Command{
	Use:     "package",
	Aliases: []string{"pkg"},
	Short:   "Package current version contents",
	Long:    "Package current version contents to a user readable package format",
	Run: func(cmd *cobra.Command, args []string) {
		var (
			flags          = cmd.Flags()
			versionName, _ = flags.GetString(ParamVersion)
			archiveFile, _ = flags.GetString(ParamArchiveFile)
			outputName, _  = flags.GetString("output")
			implName, _    = flags.GetString(ParamImpl)
			formatName, _  = flags.GetString("format")
			log            = logrus.WithFields(logrus.Fields{
				"version":        versionName,
				"archive":        archiveFile,
				"output":         outputName,
				"implementation": implName,
				"format":         formatName,
			})
		)

		format := formats[formatName]
		if format == nil {
			log.Fatal("Selected format is not recognized/implemented")
		}

		impl := archive.NewSelector().GetFactory(implName)
		if impl == nil {
			log.Fatal("Selected implementation not found in factory")
		}

		archive, err := impl.OpenArchiver(archiveFile, false)
		if err != nil {
			log.Fatalf("Open archive %q: %s", archiveFile, err)
		}

		defer impl.CloseArchiver("", false, archive)

		if err := archive.SetVersion(versionName); err != nil {
			log.Fatalf("Change archive version: %s", err)
		}

		var out io.Writer

		if outputName != "-" {
			o, err := os.OpenFile(outputName, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0o600)
			if err != nil {
				log.Fatalf("Open output file: %s", err)
			}

			defer o.Close()

			out = o
		} else {
			out = os.Stdout
		}

		if err := format(out, archive); err != nil {
			log.Fatalf("Package format: %s", err)
		}

		if o, ok := out.(*os.File); ok {
			if err := o.Close(); err != nil {
				log.Fatalf("Close output file: %s", err)
			}
		}
	},
}

var formats = map[string]func(out io.Writer, ar interfaces.IPackager) error{
	"zip": func(out io.Writer, ar interfaces.IPackager) error {
		w := zip.NewWriter(out)

		if err := ar.PackageZIP(w); err != nil {
			return fmt.Errorf("package zip: %w", err)
		}

		if err := w.Close(); err != nil {
			return fmt.Errorf("close zip file: %w", err)
		}

		return nil
	},
	"tar": func(out io.Writer, ar interfaces.IPackager) error {
		w := tar.NewWriter(out)

		if err := ar.PackageTAR(w); err != nil {
			return fmt.Errorf("package tar: %w", err)
		}

		if err := w.Close(); err != nil {
			return fmt.Errorf("close tar file: %w", err)
		}

		return nil
	},
}

func init() {
	fmts := make([]string, 0, len(formats))
	for fmt := range formats {
		fmts = append(fmts, fmt)
	}

	rootCmd.AddCommand(packageCmd)

	packageCmd.Flags().StringP(ParamVersion, "v", "", "Set version name for operation")
	packageCmd.Flags().StringP("output", "o", "-", "Output file. - is stdout")
	packageCmd.Flags().String("format", "zip", fmt.Sprintf("Output file format. Allowed formats: %+v", fmts))
}
