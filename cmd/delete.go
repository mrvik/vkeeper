package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"vkeeper/archive"
	"vkeeper/jsondto"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// deleteCmd represents the delete command
var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete a version from archive",
	Long:  `Delete the specified version from archive`,
	Run: func(cmd *cobra.Command, args []string) {
		archiveFile, _ := cmd.Flags().GetString(ParamArchiveFile)
		versionName, _ := cmd.Flags().GetString(ParamVersion)
		implName, _ := cmd.Flags().GetString(ParamImpl)
		gc, _ := cmd.Flags().GetBool("gc")
		log := logrus.WithFields(logrus.Fields{
			"version":        versionName,
			"archive":        archiveFile,
			"implementation": implName,
		})

		if err := doDelete(implName, archiveFile, versionName, gc); err != nil {
			if jsonOutput {
				json.NewEncoder(os.Stdout).Encode(jsondto.Error(err))
			}

			log.Fatal(err)
		}

		if jsonOutput {
			commandOK()
		}
	},
}

func init() {
	rootCmd.AddCommand(deleteCmd)

	deleteCmd.Flags().Bool(ParamNoCreate, false, "Exit with error if the specified archive file doesn't exist")
	deleteCmd.Flags().StringP(ParamVersion, "v", "", "Set version name for operation")
	deleteCmd.Flags().Bool("gc", true, "Do garbage collection on delete")
}

func doDelete(implName, archiveFile, versionName string, gc bool) error {
	if versionName == "" {
		return fmt.Errorf("%w: version name was not specified", ErrMissingRequiredFlag)
	}

	impl := archive.NewSelector().GetFactory(implName)
	if impl == nil {
		return fmt.Errorf("%w: %s", ErrNoFactory, implName)
	}

	archive, err := impl.OpenArchiver(archiveFile, false)
	if err != nil {
		return fmt.Errorf("open archiver: %w", err)
	}

	if err := archive.DeleteVersion(versionName); err != nil {
		return fmt.Errorf("delete version: %w", err)
	}

	if gc {
		if err := archive.GC(); err != nil {
			return fmt.Errorf("do garbage collection: %w", err)
		}
	}

	if err := impl.CloseArchiver(archiveFile, true, archive); err != nil {
		return fmt.Errorf("close archiver: %w", err)
	}

	return nil
}
