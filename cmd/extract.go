package cmd

import (
	"bytes"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path"
	"sort"
	"strings"
	"vkeeper/archive"
	"vkeeper/interfaces"
	"vkeeper/jsondto"

	"github.com/KarpelesLab/reflink"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// extractCmd represents the extract command
var extractCmd = &cobra.Command{
	Use:     "extract",
	Aliases: []string{"x"},
	Short:   "Extract files from a determinate version",
	Long: `Extract files into a determinate directory using the specified version.
The specified directory (mandatory) will be one-way synced with the zip contents.`,
	Run: func(cmd *cobra.Command, args []string) {
		var (
			flags          = cmd.Flags()
			versionName, _ = flags.GetString(ParamVersion)
			archiveFile, _ = flags.GetString(ParamArchiveFile)
			outputDir, _   = flags.GetString("output")
			implName, _    = flags.GetString(ParamImpl)
			dryRun, _      = flags.GetBool("dry-run")
			ignoreDF, _    = flags.GetBool("ignore-dotfiles")

			log = logrus.WithFields(logrus.Fields{
				"version":        versionName,
				"archive":        archiveFile,
				"output":         outputDir,
				"implementation": implName,
			})
		)

		if err := doExtract(log, implName, outputDir, archiveFile, versionName, dryRun, ignoreDF); err != nil {
			if jsonOutput {
				json.NewEncoder(os.Stdout).Encode(jsondto.Error(err))
			}

			log.Fatal(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(extractCmd)

	extractCmd.Flags().BoolP("dry-run", "n", false, "Propose changes w/o making real changes")
	extractCmd.Flags().StringP(ParamVersion, "v", "", "Set version name for operation")
	extractCmd.Flags().StringP("output", "o", "", "Output directory")
	extractCmd.Flags().Bool("ignore-dotfiles", false, "Ignore dotfiles when extracting (no delete / overwrite)")
}

func doExtract(log *logrus.Entry, implName, outputDir, archiveFile, versionName string, dryRun, ignoreDF bool) error {
	impl := archive.NewSelector().GetFactory(implName)
	if impl == nil {
		return fmt.Errorf("%w: %s", ErrNoFactory, implName)
	}

	if outputDir == "" {
		return fmt.Errorf("%w: output-dir is required", ErrMissingRequiredFlag)
	}

	archive, err := impl.OpenArchiver(archiveFile, false)
	if err != nil {
		return fmt.Errorf("open archiver: %w", err)
	}

	if err := archive.SetVersion(versionName); err != nil {
		return fmt.Errorf("select specified version: %w", err)
	}

	defer impl.CloseArchiver("", false, archive)

	os.MkdirAll(outputDir, 0o700)
	log.Info("Generating version diff summary")

	summary, err := generateDiff(log, archive, os.DirFS(outputDir), ignoreDF)
	if err != nil {
		return fmt.Errorf("generate diff: %w", err)
	}

	dumper := dumpProposedChanges
	if jsonOutput {
		dumper = dumpProposedJSON
	}

	switch {
	case log.Logger.IsLevelEnabled(logrus.InfoLevel):
		dumper(summary)
	case dryRun:
		dumper(summary)

		return nil
	}

	log.Info("Executing proposed changes")

	if err := executeProposedChanges(log, outputDir, archive, summary); err != nil {
		return fmt.Errorf("execute proposed changes: %w", err)
	}

	if jsonOutput {
		dumper(summary)
	}

	return nil
}

type DiffSummary = jsondto.DiffSummary

func generateDiff(log *logrus.Entry, ar interfaces.IArchiveFS, dir fs.FS, ignoreDF bool) (DiffSummary, error) {
	var summary DiffSummary

	versionFiles, err := ar.List()
	if err != nil {
		return summary, fmt.Errorf("extract file list from archive: %w", err)
	}

	extractMap := make(map[string]bool)

	for name := range versionFiles {
		extractMap[name] = true
	}

	if _, err := fs.Stat(dir, "."); err != nil {
		return summary, fmt.Errorf("stat output dir: %w", err)
	}

	if err := fs.WalkDir(dir, ".", func(name string, entry fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if ignoreDF && isDotFile(name) {
			if entry.IsDir() {
				return fs.SkipDir
			}

			return nil
		}

		if !entry.Type().IsRegular() {
			return nil
		}

		archiveHash, ok := versionFiles[name]
		if !ok {
			summary.Remove = append(summary.Remove, name)

			return nil
		}

		file, err := dir.Open(name)
		if err != nil {
			return fmt.Errorf("open fs file: %w", err)
		}

		defer file.Close()

		hash := sha256.New()

		if _, err := io.Copy(hash, file); err != nil {
			return fmt.Errorf("hash file %q: %w", name, err)
		}

		var c interfaces.Checksum

		copy(c[:], hash.Sum(nil))

		// We don't add to summary.Extract here bc the file is already there.
		if bytes.Equal(c[:], archiveHash[:]) {
			delete(extractMap, name)
		} else {
			log.WithField("file", name).Debugf("Hashes differ: archive=%x | filesystem=%x", archiveHash, c)
		}

		return nil
	}); err != nil {
		return summary, fmt.Errorf("walk target dir: %w", err)
	}

	for name := range extractMap {
		if ignoreDF && isDotFile(name) {
			continue
		}

		summary.Extract = append(summary.Extract, name)
	}

	sort.Strings(summary.Extract)
	sort.Strings(summary.Remove)

	return summary, nil
}

func dumpProposedChanges(ds DiffSummary) {
	out := os.Stderr

	fmt.Fprintf(out, "Diff summary: Create / Update %d files. Delete %d.\n", len(ds.Extract), len(ds.Remove))
	fmt.Fprintln(out, "\nFiles to extract")

	for _, name := range ds.Extract {
		fmt.Fprintf(out, "+ %s\n", name)
	}

	fmt.Fprintln(out, "\nFiles to remove")

	for _, name := range ds.Remove {
		fmt.Fprintf(out, "- %s\n", name)
	}
}

func dumpProposedJSON(ds DiffSummary) {
	json.NewEncoder(os.Stdout).Encode(jsondto.WithChanges(ds))
}

func executeProposedChanges(log *logrus.Entry, writeTo string, readFrom interfaces.IArchiveFS, changes DiffSummary) error {
	for _, filename := range changes.Remove {
		pth := path.Join(writeTo, filename)

		log.Debugf("Remove %q", pth)

		if err := os.Remove(pth); err != nil {
			log.Errorf("Process remotion of %q: %s", pth, err)
		}
	}

	for _, filename := range changes.Extract {
		pth := path.Join(writeTo, filename)

		log.WithFields(logrus.Fields{
			"op":   "write",
			"src":  filename,
			"dest": pth,
		}).Info("Extracting file")

		if err := writeFileTo(pth, filename, readFrom); err != nil {
			return err
		}
	}

	return nil
}

func writeFileTo(into, internalName string, ar fs.FS) error {
	if err := os.MkdirAll(path.Dir(into), 0o700); err != nil {
		return fmt.Errorf("create directories previous to %q: %w", into, err)
	}

	source, err := ar.Open(internalName)
	if err != nil {
		return fmt.Errorf("open %q to extract it's contents: %w", internalName, err)
	}

	defer source.Close()

	stat, err := source.Stat()
	if err != nil {
		return fmt.Errorf("stat archive file %q: %w", internalName, err)
	}

	dest, err := os.OpenFile(into, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, stat.Mode())
	if err != nil {
		return fmt.Errorf("open destination file %q: %w", into, err)
	}

	defer dest.Close()

	if sourceFile, ok := source.(*os.File); ok {
		err = reflink.Reflink(dest, sourceFile, true)
	} else {
		var buf [4096]byte
		_, err = io.CopyBuffer(dest, source, buf[:])
	}

	if err != nil {
		return fmt.Errorf("write archive contents to %q: %w", into, err)
	}

	if err := dest.Close(); err != nil {
		return fmt.Errorf("close dest file %q: %w", into, err)
	}

	return nil
}

func isDotFile(name string) bool {
	return len(name) > 1 && (name[0] == '.' || strings.Contains(name, "/."))
}
