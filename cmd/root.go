package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"
	"vkeeper/archive"
	"vkeeper/jsondto"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// Long parameter names.
const (
	ParamArchiveFile = "archive-file"
	ParamImpl        = "implementation"
	ParamNoCreate    = "no-create"
	ParamVersion     = "version"
)

var (
	ErrBadParams           = errors.New("bad parameters passed to command")
	ErrNoFactory           = errors.New("no factory for selected implementation")
	ErrMissingRequiredFlag = errors.New("missing required flag")
)

var rootCmd = &cobra.Command{
	Use:   "vkeeper",
	Short: "Keep your files versioned",
	Long:  `A simple version control software for seamless file version control`,
	PersistentPreRunE: func(cmd *cobra.Command, _ []string) error {
		if command := cmd.CommandPath(); strings.Contains(command, "completion") || strings.Contains(command, "help") {
			return nil
		}

		flags := cmd.Flags()
		archiveFile, _ := flags.GetString(ParamArchiveFile)
		ll, _ := flags.GetInt("log-level")

		logrus.SetLevel(logrus.Level(ll))

		if forceColor, _ := flags.GetBool("color"); forceColor {
			logrus.SetFormatter(&logrus.TextFormatter{
				ForceColors: forceColor,
			})
		}

		if archiveFile == "" {
			return fmt.Errorf("%w: %s cannot be empty", ErrBadParams, ParamArchiveFile)
		}

		if param, _ := flags.GetString(ParamImpl); param == "" {
			impl := archive.SelectImplementation(archiveFile, "zip")
			flags.Set(ParamImpl, impl)
			logrus.Debugf("Automatically selected implementation %q", impl)
		}

		return nil
	},
}

// implementationNames has the available implementation names inside the current binary.
var implementationNames = archive.NewSelector().GetNames()
var jsonOutput bool

func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	rootCmd.PersistentFlags().Int("log-level", int(logrus.WarnLevel), "Verbosity level")
	rootCmd.PersistentFlags().BoolP("color", "c", false, "Force log color output")

	rootCmd.PersistentFlags().StringP(ParamArchiveFile, "f", "", "Archive file to update. If it doesn't exist, it will be created unless --no-create is specified.")
	rootCmd.PersistentFlags().String(ParamImpl, "", fmt.Sprintf("Archiver implementation. Available implementations: %+v", implementationNames))
	rootCmd.PersistentFlags().BoolVar(&jsonOutput, "json-output", false, "Format stdout as json")
}

// Dump a generic OK JSON response if needed.
func commandOK() {
	_ = json.NewEncoder(os.Stdout).Encode(jsondto.CommonResponse{})
}
