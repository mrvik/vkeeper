package cmd

import (
	"encoding/json"
	"os"
	"time"
	"vkeeper/archive"
	"vkeeper/interfaces"
	"vkeeper/jsondto"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List versions in archive",
	Long:  `List all versions and their metadata in a table`,
	Run: func(cmd *cobra.Command, args []string) {
		archiveName, _ := cmd.Flags().GetString(ParamArchiveFile)
		implName, _ := cmd.Flags().GetString(ParamImpl)
		log := logrus.WithFields(logrus.Fields{
			"implementation": implName,
			"archive":        archiveName,
		})

		impl := archive.NewSelector().GetFactory(implName)
		if impl == nil {
			log.Fatal("Can't find selected implementation")
		}

		archive, err := impl.OpenArchiver(archiveName, false)
		if err != nil {
			log.Fatalf("Open archiver at %q: %s", archiveName, err)
		}

		defer impl.CloseArchiver(archiveName, false, archive)

		versions := archive.GetVersions()
		if jsonOutput {
			rp := jsondto.CommonResponse{
				Versions: jsondto.VersionsSlice(versions),
			}

			if err := json.NewEncoder(os.Stdout).Encode(rp); err != nil {
				log.Fatalf("Format output json: %s", err)
			}

			return
		}

		if len(versions) == 0 {
			logrus.Fatal("Archive has no versions yet")
		}

		headers, rows := formatVersions(versions)

		tableWriter := table.NewWriter()

		tableWriter.AppendHeader(headers)
		tableWriter.AppendRows(rows)

		os.Stderr.Write([]byte(tableWriter.Render()))
		os.Stderr.Write([]byte{'\n'})
	},
}

func init() {
	rootCmd.AddCommand(listCmd)
}

func formatVersions(versions []interfaces.IVersion) (table.Row, []table.Row) {
	rows := make([]table.Row, len(versions))

	for i, version := range versions {
		rows[i] = table.Row{
			version.Name(),
			version.Owner(),
			version.Created().Format(time.RFC3339),
			version.NumFiles(),
		}
	}

	return table.Row{
		"Version Name",
		"Created By",
		"Creation Time",
		"File Count",
	}, rows
}
