package dirarchive

import (
	"errors"
	"fmt"
	"vkeeper/interfaces"
	"vkeeper/refimpl"
)

var ErrFileNotFound = errors.New("file not found")

// GetVersions returns an slice of versions present in the archive
func (d *DirArchive) GetVersions() []interfaces.IVersion {
	iv := make([]interfaces.IVersion, len(d.versions))

	for i, v := range d.versions {
		iv[i] = v
	}

	return iv
}

// SetVersion establishes version as the current version.
// If the current version is not present on the list returned by GetVersions, ErrNoSuchVersion will be returned.
func (d *DirArchive) SetVersion(version string) error {
	for _, v := range d.versions {
		if v.VersionName == version {
			d.setActiveVersion(&v)

			return nil
		}
	}

	return fmt.Errorf("%w: %q", interfaces.ErrNoSuchVersion, version)
}

// NewVersion creates a new empty version in the archive. If the name specified is already present, ErrVersionCollision will be returned.
func (d *DirArchive) NewVersion(name string) error {
	vi, err := refimpl.NewVersion(name, d, func(name, user string) interfaces.IVersion {
		return refimpl.NewEmptyVersion(name, user)
	})
	if err != nil {
		return err
	}

	v := vi.(refimpl.Version)
	d.versions = append(d.versions, v)

	d.setActiveVersion(&v)

	return nil
}

func (d *DirArchive) setActiveVersion(v *refimpl.Version) {
	_ = *v // Must not be nil

	d.activeVersion = v
	d.log.WithField("activeVersion", v.VersionName).Debugf("Active version changed")
}

// DeleteVersion deletes the specified version from the archive. It returns ErrNoSuchVersion if the version cannot be found.
// It implicitly calls SetVersion over the newly created version.
func (d *DirArchive) DeleteVersion(version string) error {
	if d.activeVersion != nil && d.activeVersion.VersionName == version {
		d.activeVersion = nil
	}

	nv := make([]refimpl.Version, 0, len(d.versions))

	for _, v := range d.versions {
		if v.VersionName != version {
			nv = append(nv, v)
		}
	}

	if len(nv) == len(d.versions) {
		return fmt.Errorf("%w: %q", interfaces.ErrNoSuchVersion, version)
	}

	d.versions = nv

	return nil
}
