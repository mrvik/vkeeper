package dirarchive

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path"
	"vkeeper/refimpl"

	"github.com/sirupsen/logrus"
)

type DirArchive struct {
	versions      []refimpl.Version
	activeVersion *refimpl.Version
	blobs         *refimpl.BlobManager

	versionsDir string
	objectsDir  string

	log *logrus.Entry
}

func NewDirArchive(baseDir string, create bool) (*DirArchive, error) {
	log := logrus.WithFields(logrus.Fields{
		"implementation": "dirarchive",
		"archive":        baseDir,
	})
	versionsDir := path.Join(baseDir, refimpl.DirectoryStore)
	objectsDir := path.Join(baseDir, refimpl.ObjectStore)

	if create {
		if err := createStructure(versionsDir, objectsDir); err != nil {
			return nil, err
		}
	}

	log.Info("Reading versions from directory")

	versions, err := refimpl.ReadVersions(os.DirFS(versionsDir))
	if err != nil {
		return nil, fmt.Errorf("read version files: %w", err)
	}

	blobManager := new(refimpl.BlobManager)

	blobs, err := os.ReadDir(objectsDir)
	if err != nil {
		return nil, fmt.Errorf("read blobs dir: %w", err)
	}

	log.Infof("Populating BlobManager with %d blobs", len(blobs))
	blobManager.Populate(blobs)
	log.Debug("DirArchive initialized")

	return &DirArchive{
		versions: versions,
		blobs:    blobManager,
		log:      log,

		versionsDir: versionsDir,
		objectsDir:  path.Join(baseDir, refimpl.ObjectStore),
	}, nil
}

func createStructure(dirnames ...string) error {
	for _, d := range dirnames {
		if e := os.MkdirAll(d, 0o700); e != nil {
			return fmt.Errorf("create structure: %w", e)
		}
	}

	return nil
}

func (da *DirArchive) WriteTo(io.Writer) (int64, error) {
	currentVersions := make(map[string]struct{}, len(da.versions))

	for _, v := range da.versions {
		currentVersions[v.VersionName] = struct{}{}

		if err := writeVersion(path.Join(da.versionsDir, v.VersionName), v); err != nil {
			return 0, fmt.Errorf("write version %q: %w", v.VersionName, err)
		}
	}

	versionFiles, err := os.ReadDir(da.versionsDir)
	if err != nil {
		return 0, fmt.Errorf("list version files: %w", err)
	}

	for _, version := range versionFiles {
		name := path.Base(version.Name())

		if _, ok := currentVersions[name]; !ok {
			os.Remove(path.Join(da.versionsDir, name))
		}
	}

	return 0, nil
}

func (*DirArchive) Close() error {
	return nil
}

func writeVersion(path string, version refimpl.Version) error {
	file, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0o600)
	if err != nil {
		return fmt.Errorf("open version file: %w", err)
	}

	defer file.Close()

	if err := json.NewEncoder(file).Encode(version); err != nil {
		return fmt.Errorf("encode version: %w", err)
	}

	return nil
}
