package dirarchive

import (
	"archive/tar"
	"archive/zip"
	"vkeeper/refimpl"
)

// PackageZIP generates a new zip file from the files present on the active version.
func (d *DirArchive) PackageZIP(to *zip.Writer) error {
	return refimpl.PackageZIP(d, to)
}

// PackageTAR writes all files present on the active version to the specified tar writer.
func (d *DirArchive) PackageTAR(to *tar.Writer) error {
	return refimpl.PackageTAR(d, to)
}
