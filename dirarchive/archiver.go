package dirarchive

import (
	"encoding/hex"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path"
	"vkeeper/interfaces"
	"vkeeper/refimpl"

	"github.com/KarpelesLab/reflink"
)

// Open opens the specified file on the current version.
// It will always return a fs.PathError as specified by fs.FS.
// Open must be safe to call concurrently.
func (d *DirArchive) Open(filename string) (fs.File, error) {
	if d.activeVersion == nil {
		return nil, fmt.Errorf("open %q: %w", filename, interfaces.ErrNoVersion)
	}

	info, err := d.activeVersion.FindFile(filename)
	if err != nil {
		return nil, fmt.Errorf("open %q: %w", filename, err)
	}

	blob, err := os.Open(path.Join(d.objectsDir, hex.EncodeToString(info.SHA256[:])))
	if err != nil {
		return nil, fmt.Errorf("open %q: open blob %x: %w", filename, info.SHA256, err)
	}

	return refimpl.NewFileInfoWrapper(blob, info), nil
}

// Store a new file in the archive. Filename will be added to the index replacing any file with the specified filename.
// Must be safe for concurrent use.
func (d *DirArchive) Store(filename string, hash interfaces.Checksum, file fs.File) error {
	defer file.Close()

	if d.activeVersion == nil {
		return fmt.Errorf("store %q: %w", filename, interfaces.ErrNoVersion)
	}

	stat, err := file.Stat()
	if err != nil {
		return fmt.Errorf("store %q: %w", filename, err)
	}

	// Update version metadata
	d.activeVersion.AddFile(filename, hash, stat)

	strhash := hex.EncodeToString(hash[:])

	if !d.blobs.HasOrRegister(hash) {
		return nil
	}

	// Write new blob file
	dest, err := os.OpenFile(path.Join(d.objectsDir, strhash), os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0o600)
	if err != nil {
		return fmt.Errorf("store %q: open object %x dest file: %w", filename, hash, err)
	}

	defer dest.Close()

	if src, ok := file.(*os.File); ok {
		// This may boost this archiver on CoW filesystems.
		// If reflinking is not supported, will fall back to io.Copy.
		err = reflink.Reflink(dest, src, true)
	} else {
		_, err = io.Copy(dest, file)
	}

	if err != nil {
		return fmt.Errorf("copy / link file %q to objects dir: %w", filename, err)
	}

	return nil
}

// Delete removes a file from index. Blob is left on the tree unless GC is called.
// Must be safe for concurrent use.
func (d *DirArchive) Delete(filename string) error {
	// Update version metadata
	if d.activeVersion == nil {
		return fmt.Errorf("delete %q: %w", filename, interfaces.ErrNoVersion)
	}

	d.activeVersion.RemoveFile(filename)
	return nil
}

// GC runs a garbage collection by looking up orphan hashes.
// It won't do the heavy lifting to remove files. Close **must** be called in order to actually do changes.
func (d *DirArchive) GC() error {
	d.log.Info("Perform GC")

	usedBlobs := make(map[string]struct{})

	for _, v := range d.versions {
		for _, checksum := range v.List() {
			usedBlobs[hex.EncodeToString(checksum[:])] = struct{}{}
		}
	}

	currentBlobs, err := os.ReadDir(d.objectsDir)
	if err != nil {
		return fmt.Errorf("gc: get current blobs: %w", err)
	}

	var errors []error

	for _, blobEntry := range currentBlobs {
		basename := path.Base(blobEntry.Name())
		if _, inUse := usedBlobs[basename]; inUse {
			continue
		}

		d.log.Debugf("Remove blob %q", basename)

		if err := os.Remove(path.Join(d.objectsDir, basename)); err != nil {
			errors = append(errors, err)
		}
	}

	if len(errors) > 0 {
		return fmt.Errorf("do GC: %w", interfaces.MultiError(errors))
	}

	return nil
}

// List returns a map of files and their checksum.
func (d *DirArchive) List() (map[string]interfaces.Checksum, error) {
	// Update version metadata
	if d.activeVersion == nil {
		return nil, fmt.Errorf("list blobs: %w", interfaces.ErrNoVersion)
	}

	return d.activeVersion.List(), nil
}

// Check for internal consistency inside the archive.
// Returns error if any error is encontered on the internal packaging.
func (d *DirArchive) Check() error {
	base := os.DirFS(d.objectsDir)

	return refimpl.CheckBlobs(d.log, base)
}
