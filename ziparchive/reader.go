package ziparchive

import (
	"archive/zip"
	"fmt"
	"io"
	"io/fs"
	"vkeeper/interfaces"
	"vkeeper/refimpl"

	"github.com/sirupsen/logrus"
)

// Implementations in this file.
var _ interfaces.IVersioner = Reader{}

type Reader struct {
	zip    *zip.Reader
	format *Format
	log    *logrus.Entry
}

type ReadCloserSeeker interface {
	io.ReaderAt
	fs.File
}

func NewReader(filename string, from ReadCloserSeeker) (Reader, error) {
	stat, err := from.Stat()
	if err != nil {
		return Reader{}, fmt.Errorf("stat archive: %w", err)
	}

	tr, err := zip.NewReader(from, stat.Size())
	if err != nil {
		return Reader{}, fmt.Errorf("open archive: %w", err)
	}

	fm, err := NewFormat(tr)
	if err != nil {
		return Reader{}, err
	}

	return Reader{
		zip:    tr,
		format: fm,
		log: logrus.WithFields(logrus.Fields{
			"implementation": "zip",
			"archive":        filename,
		}),
	}, nil
}

// NewEmptyReader initializes an empty file with the needed structures.
// Filename like in NewReader is for merely informative purposes on logs.
func NewEmptyReader(filename string) Reader {
	return Reader{
		zip: &zip.Reader{
			File:    []*zip.File{},
			Comment: "",
		},
		format: NewEmptyFormat(),
		log: logrus.WithFields(logrus.Fields{
			"implementation": "zip (new)",
			"archive":        filename,
		}),
	}
}

// GetVersions returns an slice of versions present in the archive
func (r Reader) GetVersions() []interfaces.IVersion {
	versions := make([]interfaces.IVersion, len(r.format.Versions))

	for i, v := range r.format.Versions {
		versions[i] = v
	}

	return versions
}

// SetVersion establishes version as the current version.
// If the current version is not present on the list returned by GetVersions, ErrNoSuchVersion will be returned.
func (r Reader) SetVersion(version string) error {
	for _, v := range r.format.Versions {
		if v.VersionName == version {
			r.setActiveVersion(&v)

			return nil
		}
	}

	return fmt.Errorf("%w: %s", interfaces.ErrNoSuchVersion, version)
}

func (r Reader) setActiveVersion(v *Version) {
	r.format.ActiveVersion = v
	r.log.WithField("currentVersion", v.VersionName).Info("Updated active version")
}

// NewVersion creates a new empty version in the archive. If the name specified is already present, ErrVersionCollision will be returned.
func (r Reader) NewVersion(name string) error {
	vi, err := refimpl.NewVersion(name, r, func(name, user string) interfaces.IVersion {
		return NewEmptyVersion(name, user)
	})
	if err != nil {
		return err
	}

	v := vi.(Version)
	r.format.Versions = append(r.format.Versions, v)

	r.log.WithField("versionName", v.Name).Info("Created new version")
	r.setActiveVersion(&v)

	return nil
}

// DeleteVersion deletes the specified version from the archive. It returns ErrNoSuchVersion if the version cannot be found.
// It implicitly calls SetVersion over the newly created version.
func (r Reader) DeleteVersion(version string) error {
	otherVersions := make([]Version, 0, len(r.format.Versions))

	for _, v := range r.format.Versions {
		if v.VersionName == version {
			continue
		}

		otherVersions = append(otherVersions, v)
	}

	if len(otherVersions) == len(r.format.Versions) {
		return fmt.Errorf("delete version %q: %w", version, interfaces.ErrNoSuchVersion)
	}

	r.format.Versions = otherVersions

	return nil
}
