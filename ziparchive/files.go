package ziparchive

import (
	"archive/zip"
	"encoding/hex"
	"fmt"
	"io"
	"io/fs"
	"vkeeper/interfaces"
	"vkeeper/refimpl"
)

// Implementations in this file
var (
	_ fs.FS                 = Reader{}
	_ interfaces.IArchiveFS = Reader{}
)

// Open opens the named file.
//
// When Open returns an error, it should be of type *PathError
// with the Op field set to "open", the Path field set to name,
// and the Err field describing the problem.
//
// Open should reject attempts to open names that do not satisfy
// ValidPath(name), returning a *PathError with Err set to
// ErrInvalid or ErrNotExist.
func (r Reader) Open(name string) (fs.File, error) {
	if !fs.ValidPath(name) {
		return nil, &fs.PathError{Op: "open", Path: name, Err: fs.ErrInvalid}
	}

	if r.format.ActiveVersion == nil {
		return nil, &fs.PathError{Op: "open", Path: name, Err: interfaces.ErrNoVersion}
	}

	blobName := r.format.ActiveVersion.findFile(name)

	var err error
	if blobName == nil {
		err = fs.ErrNotExist
	} else if _, ok := r.format.Blobs[*blobName]; !ok {
		err = fmt.Errorf("blob %q: %w", *blobName, fs.ErrNotExist)
	}

	if err != nil {
		return nil, &fs.PathError{Op: "open", Path: name, Err: err}
	}

	blobSub, _ := fs.Sub(r.zip, ObjectStore)

	file, err := openObject(blobSub, *blobName)
	if err != nil {
		return nil, &fs.PathError{Op: "open", Path: name, Err: err}
	}

	return refimpl.NewFileInfoWrapper(file, *r.format.ActiveVersion.Files[name]), nil
}

func openObject(f fs.FS, sum interfaces.Checksum) (fs.File, error) {
	return f.Open(hex.EncodeToString(sum[:]))
}

// Store a new file in the archive. Filename will be added to the index replacing any file with the specified filename.
func (r Reader) Store(filename string, hash interfaces.Checksum, file fs.File) error {
	if r.format.ActiveVersion == nil {
		return &fs.PathError{Op: "store", Path: filename, Err: interfaces.ErrNoVersion}
	}

	info, err := file.Stat()
	if err != nil {
		return fmt.Errorf("read file info for index %q: %w", filename, err)
	}

	r.format.ActiveVersion.putFile(filename, hash, info)

	if r.format.blobExists(hash) {
		file.Close()

		return nil
	}

	return r.format.addBlobFrom(hash, file)
}

// Delete removes a file from index. Blob is left on the tree unless GC is called.
func (r Reader) Delete(filename string) error {
	if r.format.ActiveVersion == nil {
		return &fs.PathError{Op: "delete", Path: filename, Err: interfaces.ErrNoVersion}
	}

	return r.format.ActiveVersion.deleteFile(filename)
}

// GC runs a garbage collection by looking up orphan hashes.
// It won't do the heavy lifting to remove files. Close **must** be called in order to actually do changes.
func (r Reader) GC() error {
	log := r.log.WithField("op", "GC")
	blobDir, _ := fs.Sub(r.zip, ObjectStore)
	usedBlobs := r.format.getAllUsedBlobs()
	remove := make([]interfaces.Checksum, 0)

	for blob := range r.format.Blobs {
		if _, ok := usedBlobs[blob]; !ok {
			remove = append(remove, blob)
		}
	}

	log.Infof("Marking %d blobs for deletion", len(remove))

	for _, b := range remove {
		log.Debugf("Marking blob %x for deletion", b)
		r.format.removeBlob(b)
	}

	log.Info("Updating file index properties")

	for _, version := range r.format.Versions {
		for filename, versionFile := range version.Files {
			if np := getProperties(*versionFile, blobDir); np != nil {
				log.Debugf("Updating file info for %s (%d bytes)", filename, np.FileSize)

				versionFile.UpdateInfo(*np)
			}
		}
	}

	return nil
}

func (r Reader) Check() error {
	blobFS, err := fs.Sub(r.zip, ObjectStore)
	if err != nil {
		return fmt.Errorf("open object store: %w", err)
	}

	return refimpl.CheckBlobs(r.log, blobFS)
}

func getProperties(file refimpl.File, blobDir fs.FS) *refimpl.FileInfo {
	blob, err := openObject(blobDir, file.SHA256)
	if err != nil {
		return nil
	}

	blobStat, err := blob.Stat()
	if err != nil {
		return nil
	}

	return &refimpl.FileInfo{
		Mode:     file.FileInfo.Mode,
		Mtime:    file.FileInfo.Mtime,
		FileSize: blobStat.Size(),
	}
}

// List returns a map of files and their checksum.
func (r Reader) List() (map[string]interfaces.Checksum, error) {
	if r.format.ActiveVersion == nil {
		return nil, interfaces.ErrNoVersion
	}

	mp := make(map[string]interfaces.Checksum, len(r.format.ActiveVersion.Files))

	for name, file := range r.format.ActiveVersion.Files {
		mp[name] = file.SHA256
	}

	return mp, nil
}

func (r Reader) WriteTo(w io.Writer) (n int64, err error) {
	zipw := zip.NewWriter(w)
	defer zipw.Close()

	r.log.Info("Writing versions")

	if err := r.format.writeVersions(zipw); err != nil {
		return 0, fmt.Errorf("write version files: %w", err)
	}

	r.log.Info("Writing blob files")

	if err := r.writeBlobs(zipw); err != nil {
		return 0, fmt.Errorf("write blob files: %w", err)
	}

	return 0, nil
}

// Close does nothing as this Reader has nothing to do on close.
// Call WriteTo if you want to persist the current state.
func (r Reader) Close() error {
	for _, f := range r.format.Summary.PutAlready {
		if c, ok := f.(io.Closer); ok {
			c.Close()
		}
	}

	return nil
}
