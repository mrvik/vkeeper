package ziparchive

import (
	"archive/zip"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path"
	"vkeeper/interfaces"

	"github.com/sirupsen/logrus"
)

type Sizer interface {
	Size() int64
}

func (f Format) addBlobFrom(hash interfaces.Checksum, reader io.Reader) error {
	f.blobMtx.Lock()
	defer f.blobMtx.Unlock()

	blobName := path.Join(ObjectStore, hex.EncodeToString(hash[:]))

	if file, ok := reader.(*os.File); ok && path.IsAbs(file.Name()) {
		defer file.Close()

		f.Summary.PutDelayed[blobName] = file.Name()

		return nil
	}

	f.Summary.PutAlready[blobName] = reader
	f.Blobs[hash] = struct{}{}

	return nil
}

func (f Format) blobExists(hash interfaces.Checksum) bool {
	f.blobMtx.RLock()
	defer f.blobMtx.RUnlock()

	_, ok := f.Blobs[hash]

	return ok
}

func (f Format) removeBlob(hash interfaces.Checksum) {
	f.Summary.Remove[hex.EncodeToString(hash[:])] = struct{}{}
}

func (f Format) getAllUsedBlobs() map[interfaces.Checksum]struct{} {
	mp := make(map[interfaces.Checksum]struct{}, len(f.Blobs))

	for _, v := range f.Versions {
		for _, file := range v.Files {
			mp[file.SHA256] = struct{}{}
		}
	}

	return mp
}

func (r Reader) writeBlobs(to *zip.Writer) error {
	r.log.Debugf("Writing %d old blobs (at max)", len(r.zip.File))

	for _, file := range r.zip.File {
		name := path.Base(file.Name)
		check, err := parseHex(name)
		_, nameIsBlob := r.format.Blobs[check]
		_, nameIsDeleted := r.format.Summary.Remove[name]

		if err != nil || !nameIsBlob || nameIsDeleted {
			continue
		}

		if err := copyExistingBlob(to, file); err != nil {
			return fmt.Errorf("copy blob %q: %w", file.Name, err)
		}
	}

	r.log.Infof("Writing %d new blobs (already open)", len(r.format.Summary.PutAlready))

	for name, reader := range r.format.Summary.PutAlready {
		if err := copyBlob(to, name, reader); err != nil {
			return err
		}
	}

	r.log.Infof("Writing %d new blobs (not open)", len(r.format.Summary.PutDelayed))

	for name, reader := range r.format.Summary.PutDelayed {
		if err := openCopyBlob(to, name, reader); err != nil {
			return err
		}
	}
	return nil
}

func openCopyBlob(to *zip.Writer, blobName, filename string) error {
	logrus.Debugf("Open file %q to store", filename)

	file, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("open original file to copy: %w", err)
	}

	defer file.Close()

	return copyBlob(to, blobName, file)
}

func copyExistingBlob(to *zip.Writer, file *zip.File) error {
	reader, err := file.Open()
	if err != nil {
		return fmt.Errorf("open blob %q: %w", file.Name, err)
	}

	defer reader.Close()

	return copyBlob(to, file.Name, reader)
}

func copyBlob(to *zip.Writer, path string, reader io.Reader) error {
	newFile, err := to.Create(path)
	if err != nil {
		return fmt.Errorf("create blob %q on new zip: %w", path, err)
	}

	var buf [4096]byte

	if _, err := io.CopyBuffer(newFile, reader, buf[:]); err != nil {
		return fmt.Errorf("write blob %q on new zip: %w", path, err)
	}

	return nil
}
