package ziparchive

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"sync"
	"time"
	"vkeeper/interfaces"
	"vkeeper/refimpl"
)

// Implementations in this file
var _ interfaces.IVersion = Version{}

var ErrNoVersions = errors.New("no versions in archive")

// Directories in archive root.
const (
	ObjectStore    = refimpl.ObjectStore
	DirectoryStore = refimpl.DirectoryStore
)

type Format struct {
	Versions      []Version
	ActiveVersion *Version

	// Blobs holds a map of the blob names so we're able to do fast lookups.
	Blobs map[interfaces.Checksum]struct{}

	// Summary represents a summary of files to be added, removed or deleted on close.
	Summary Summary

	blobMtx *sync.RWMutex
}

func NewFormat(base fs.FS) (*Format, error) {
	versionsDir, err := fs.Sub(base, DirectoryStore)
	if err != nil {
		return nil, fmt.Errorf("pivot to versions directory: %w", err)
	}

	versionFiles, err := fs.ReadDir(versionsDir, ".")
	if err != nil {
		return nil, fmt.Errorf("read version files: %w", err)
	}

	if len(versionFiles) == 0 {
		return nil, ErrNoVersions
	}

	versions, err := readVersions(versionsDir, versionFiles)
	if err != nil {
		return nil, nil
	}

	blobFiles, err := fs.ReadDir(base, ObjectStore)
	if err != nil {
		return nil, fmt.Errorf("open blob dir: %w", err)
	}

	blobMap := make(map[interfaces.Checksum]struct{}, len(blobFiles))

	for _, blob := range blobFiles {
		sha, err := parseHex(blob.Name())
		if err != nil {
			return nil, err
		}

		blobMap[sha] = struct{}{}
	}

	return &Format{
		ActiveVersion: nil,
		Versions:      versions,
		Blobs:         blobMap,
		Summary:       NewSummary(),

		blobMtx: new(sync.RWMutex),
	}, nil
}

func NewEmptyFormat() *Format {
	return &Format{
		Versions: []Version{},
		Blobs:    map[interfaces.Checksum]struct{}{},
		Summary:  NewSummary(),

		blobMtx: new(sync.RWMutex),
	}
}

type Version struct {
	VersionName string `json:"name"`

	// File index for this version.
	Files     map[string]*refimpl.File `json:"files"`
	Ctime     time.Time                `json:"ctime"`
	CreatedBy string                   `json:"owner"`

	mtx *sync.RWMutex
}

func NewVersion(name string, f io.ReadCloser) (Version, error) {
	defer f.Close()

	v := Version{
		mtx: new(sync.RWMutex),
	}

	if err := json.NewDecoder(f).Decode(&v); err != nil {
		return v, fmt.Errorf("read description for version %s: %w", name, err)
	}

	return v, nil
}

func NewEmptyVersion(name, creator string) Version {
	return Version{
		VersionName: name,
		Files:       make(map[string]*refimpl.File),
		Ctime:       time.Now(),
		CreatedBy:   creator,

		mtx: new(sync.RWMutex),
	}
}

func (v Version) Name() string {
	return v.VersionName
}

func (v Version) Owner() string {
	return v.CreatedBy
}

func (v Version) Created() time.Time {
	return v.Ctime
}

func (v Version) NumFiles() int64 {
	return int64(len(v.Files))
}

func readVersions(versionsDir fs.FS, versionFiles []fs.DirEntry) ([]Version, error) {
	versions := make([]Version, 0, len(versionFiles))

	for _, versionFile := range versionFiles {
		if versionFile.IsDir() {
			continue
		}

		vfile, err := versionsDir.Open(versionFile.Name())
		if err != nil {
			return nil, fmt.Errorf("open version %s: %w", versionFile.Name(), err)
		}

		version, err := NewVersion(versionFile.Name(), vfile)
		if err != nil {
			return nil, fmt.Errorf("parse version file %s: %w", versionFile.Name(), err)
		}

		versions = append(versions, version)
	}

	return versions, nil
}

func parseHex(blobName string) (interfaces.Checksum, error) {
	var sha interfaces.Checksum

	encodedSHA, err := hex.DecodeString(blobName)
	if err != nil {
		return sha, fmt.Errorf("malformed sha: %w", err)
	}

	copy(sha[:], encodedSHA)

	return sha, nil
}
