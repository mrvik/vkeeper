package ziparchive

import (
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"path"
	"vkeeper/interfaces"
	"vkeeper/refimpl"
)

func (v *Version) putFile(filename string, hash interfaces.Checksum, info fs.FileInfo) {
	v.mtx.Lock()
	defer v.mtx.Unlock()

	v.Files[filename] = &refimpl.File{
		SHA256: hash,
		FileInfo: refimpl.FileInfo{
			Mode:     info.Mode(),
			Mtime:    info.ModTime(),
			FileSize: info.Size(),
		},
	}
}

func (v *Version) deleteFile(filename string) error {
	if _, ok := v.Files[filename]; !ok {
		return &fs.PathError{Op: "delete", Path: filename, Err: fs.ErrNotExist}
	}

	delete(v.Files, filename)

	return nil
}

func (v Version) findFile(filename string) *interfaces.Checksum {
	f, ok := v.Files[filename]
	if !ok {
		return nil
	}

	return &f.SHA256
}

type ArchiveWriter interface {
	Create(name string) (io.Writer, error)
}

func (f Format) writeVersions(to ArchiveWriter) error {
	for _, v := range f.Versions {
		writer, err := to.Create(path.Join(DirectoryStore, v.VersionName))
		if err != nil {
			return fmt.Errorf("create file for version %s: %w", v.VersionName, err)
		}

		if err := json.NewEncoder(writer).Encode(v); err != nil {
			return fmt.Errorf("write version %s: %w", v.VersionName, err)
		}
	}

	return nil
}
