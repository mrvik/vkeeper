package ziparchive

import (
	"archive/tar"
	"archive/zip"
	"fmt"
	"vkeeper/refimpl"
)

// PackageZIP generates a new zip file from the files present on the active version.
func (r Reader) PackageZIP(to *zip.Writer) error {
	if err := refimpl.PackageZIP(r, to); err != nil {
		return fmt.Errorf("package zip file: %w", err)
	}

	return nil
}

// PackageTAR writes all files present on the active version to the specified tar writer.
func (r Reader) PackageTAR(to *tar.Writer) error {
	if err := refimpl.PackageTAR(r, to); err != nil {
		return fmt.Errorf("package tar file: %w", err)
	}

	return nil
}
