package ziparchive

import (
	"io"
)

type Summary struct {
	PutDelayed map[string]string
	PutAlready map[string]io.Reader
	Remove     map[string]struct{}
}

// NewSummary creates an empty Summary with its maps initilized.
func NewSummary() Summary {
	return Summary{
		PutAlready: make(map[string]io.Reader),
		PutDelayed: make(map[string]string),
		Remove:     make(map[string]struct{}),
	}
}
